use super::model::Model;

#[derive(Debug)]
pub struct Metamodel {
    pub models: Vec<Model>,
}
