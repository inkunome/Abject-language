use std::fmt::Debug;

#[derive(Debug)]
pub struct Code {
    pub statements: Vec<Statement>,
}

#[derive(Debug)]
pub enum Statement {
    Call(Call),
    Return(Return),
}

#[derive(Debug)]
pub enum Expression {
    String(String),
    Int(u32),
}

#[derive(Debug)]
pub struct Call {
    pub value: Expression,
    pub identifier: String,
}

#[derive(Debug)]
pub struct Return {
    pub value: Expression,
}
