use pest::{iterators::Pair, Parser};

#[derive(Parser)]
#[grammar = "parser/abject.pest"]
pub struct AbjectParser;

use crate::model::code::{Call, Code, Expression, Return, Statement};
use crate::model::metamodel::Metamodel;
use crate::model::model::Model;

use std::{fs, vec};

pub fn parse(path: &str) -> Metamodel {
    let unparsed_file = fs::read_to_string(path).expect("cannot read file");

    let file = AbjectParser::parse(Rule::program, &unparsed_file)
        .expect("unsuccessful parse")
        .next()
        .unwrap();

    let mut metamodel = Metamodel { models: vec![] };

    for record in file.into_inner() {
        match record.as_rule() {
            Rule::top_level_statement => {
                for top in record.into_inner() {
                    match top.as_rule() {
                        Rule::facet_declaration => {
                            let identifier = top.into_inner().nth(0).unwrap();

                            println!(">>> function {:?}", identifier.as_str());
                        }
                        Rule::model_declaration => {
                            let identifier = top.into_inner().nth(0).unwrap();

                            let model = Model {
                                identifier: identifier.as_str().to_string(),
                            };

                            metamodel.models.push(model);
                        }
                        Rule::define_declaration => {
                            let mut inner = top.into_inner();
                            let identifier = inner.nth(0).unwrap();
                            let body = inner.nth(0).unwrap();

                            println!(">>> define {:?}", identifier.as_str());
                            println!(">>> body {:?}", compile(body));
                        }
                        Rule::use_declaration => {
                            let identifier = top.into_inner().nth(0).unwrap();

                            println!(">>> use {:?}", identifier.as_str());
                        }
                        _ => unreachable!(),
                    }
                }
            }
            _ => unreachable!(),
        }
    }

    metamodel
}

fn compile(body: Pair<Rule>) -> Code {
    let mut result = Code { statements: vec![] };

    for statement in body.into_inner() {
        match statement.as_rule() {
            Rule::statement_expression => {
                for inner in statement.into_inner() {
                    match inner.as_rule() {
                        Rule::call_expression => {
                            let mut inner = inner.into_inner();

                            let value = expression(inner.nth(0).unwrap());
                            let identifier = inner.nth(0).unwrap().as_str().to_string();

                            result
                                .statements
                                .push(Statement::Call(Call { value, identifier }))
                        }
                        _ => unreachable!(),
                    }
                }
            }
            Rule::statement_return => {
                let mut inner = statement.into_inner();

                result.statements.push(Statement::Return(Return {
                    value: expression(inner.nth(0).unwrap()),
                }))
            }
            _ => unreachable!(),
        }
    }

    result
}

fn expression(value: Pair<Rule>) -> Expression {
    match value.as_rule() {
        Rule::quoted_string => Expression::String(String::from(value.as_str())),
        Rule::single_expression => expression(value.into_inner().nth(0).unwrap()),
        Rule::number => Expression::Int(value.as_str().parse().unwrap()),
        _ => unreachable!(),
    }
}
