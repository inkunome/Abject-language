extern crate pest;
#[macro_use]
extern crate pest_derive;

use llama::*;
use std::error::Error;

use crate::codegen::codegen::CodeGen;
use crate::grammar::parse;

pub mod codegen;
pub mod grammar;
pub mod model;

fn main() -> Result<(), Box<dyn Error>> {
    let mut jit = Jit::new("main", None)?;

    let metamodel = parse("sample/hello.abj");

    println!(">>> metamodel {:?}", metamodel);

    let x = 1;
    let y = 2;
    let z = 3;

    /*unsafe {
        println!("{} + {} + {} = {}", x, y, z, sum.call(x, y, z));
        assert_eq!(sum.call(x, y, z), x + y + z);
    }*/

    Ok(())
}
